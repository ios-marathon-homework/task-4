import UIKit
import Darwin
import Foundation

//#1
enum Figure : String {
    case King = "King"
    case Queen = "Queen"
    case Knight = "Knight"
    case Rook = "Rook"
    case Bishop = "Bishop"
    case Pawns = "Pawns"
    
    enum Color : String {
        case White = "White", Black = "Black"
    }
    
}
//---

//#2
typealias Position = (y: Int, x: String)
typealias Chess = (name: Figure, color: Figure.Color, pos: Position)

var kingWhite : Chess = (Figure.King, Figure.Color.White, (1, "A"))
var queenWhite : Chess = (Figure.Queen, Figure.Color.White, (2, "A"))
var queenBlack : Chess = (Figure.King, Figure.Color.Black, (3, "A"))

//var kingWhite : Chess = (Figure.King, Figure.Color.White, ("A", 4))
//var kingWhite : Chess = (Figure.King, Figure.Color.White, ("A", 5))
//var kingWhite : Chess = (Figure.King, Figure.Color.White, ("A", 6))

var arrayOfFigures : [Chess] = [kingWhite, queenBlack, queenWhite]

func printFigures() {
    
for i in arrayOfFigures {
    print((i.name.rawValue), (i.color.rawValue), "Pos \(i.pos.x)\(i.pos.y)")
    }
}

printFigures()
//---
print()

//#3
var deckArray = Array(repeating:Array(repeating:String(), count: 8), count: 8)
var dictionary = ["A" : 1, "B" : 2, "C" : 3, "D" : 4, "E" : 5, "F" : 6, "G" : 7, "H" : 8]
func deck(figures:[Chess]) {
    print("")
    for i in 0...7 {
        for j in 0...7 {
            switch(i,j) {
            case let (i,j) where i % 2 != j % 2 :
                
                deckArray[i][j] = "□"
            case _:
                deckArray[i][j] = "■"
            }
        }
    }
    
    for i in figures {
        switch i {
        case let i where i.name == Figure.King  : deckArray[dictionary[i.pos.x]! - 1][i.pos.y - 1] = (i.color == Figure.Color.White) ? "\u{2654}" : "\u{265A}"
        case let i where i.name == Figure.Queen : deckArray[dictionary[i.pos.x]! - 1][i.pos.y - 1] = (i.color == Figure.Color.White) ? "\u{2655}" : "\u{265B}"
        case let i where i.name == Figure.Queen : deckArray[dictionary[i.pos.x]! - 1][i.pos.y - 1] = (i.color == Figure.Color.White) ? "\u{2655}" : "\u{265B}"
        
        default: break
        }
    }
    print("A B C D E F G H ")
    for i in 0...7 {

        for j in  0...7 {
            print(deckArray[j][i], terminator: " ")
        }
        print(i + 1)
    }
}

deck(figures: arrayOfFigures)

print("\nNo checkmate")
//---

//#4
func changeFigurePos(`var` figure: Chess, pos: Position) {
    var newFigure = figure
    switch pos {
    case (1...8,"A"..."H"): newFigure.pos = pos
        for (index, value) in arrayOfFigures.enumerated() {
            if value.name == newFigure.name && value.color == newFigure.color {
                arrayOfFigures[index] = newFigure
            }
        }
    default: print("Incorrect")
    }
    deck(figures: arrayOfFigures)
}

changeFigurePos(var: kingWhite, pos: (6, "H"))
//---

//func blankArray(array : [[String]]) -> [[String]] {
//    var color = false
//    var mutableAray = array
//    for i in 0...7 {
//        for j in 0...7 {
//            if !color {
//                mutableAray[i][j] = "B "
//                color.toggle()
//            }
//            else {
//                mutableAray[i][j] = "W "
//                color.toggle()
//            }
//        }
//        color.toggle()
//    }
//    return mutableAray
//}
//
//func printSides(array : [[String]]) {
//    let constArray = array
//    var vert = 8
//    let hor = "  a b c d e f g h"
//
//    for i in 0...7 {
//        print(vert, terminator: " ")
//        vert -= 1
//        for j in 0...7 {
//            print(constArray[i][j], terminator: "")
//        }
//        print()
//    }
//    print(hor)
//}
//
//func printDeck(array : [[String]]) -> [[String]] {
//
//    let constArray = array
//    var vert = 8
//    let hor = "  A B C D E F G H"
//    var color = false
//    var mutableAray = array
//
//    for i in 0...7 {
//        print(vert, terminator: " ")
//        vert -= 1
//        for j in 0...7 {
//            print(constArray[i][j], terminator: "")
//            if !color {
//                mutableAray[i][j] = "■ "
//                print(mutableAray[i][j], terminator: "")
//                color.toggle()
//            }
//            else {
//                mutableAray[i][j] = "□ "
//                print(mutableAray[i][j], terminator: "")
//                color.toggle()
//            }
//        }
//        print()
//        color.toggle()
//    }
//        print(hor)
//
//    return mutableAray
//}
//
////Deck = blankArray(array: Deck)
////printSides(array: Deck)
//
//printDeck(array: Deck)
//
//
//func setFigures (array : [Chess], arrayWithFigures : [[String]]) -> [[String]] {
//
//    var arrayDeck = arrayWithFigures
//
//
//    for i in array {
//        var vert = 0
//        var hor = 0
//        var figure : String
//
//        switch i.pos {
//        case
//        }
//    }
//
//}



